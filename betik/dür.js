const uglify = require('uglify-js-es6')
const rollup = require('rollup')
const zlib = require('zlib')
const fs = require('fs')
const read = f => fs.readFileSync(f,'utf-8')
const write = (f, d) => fs.writeFileSync(f, d)

require('../index')
let sürümAna = require('./anaSürüm.json')
let sürüm = require('./../package.json').version
let README = read('./betik/README.MD')

let açıklama = '/* 2017 - 2018 ⊕ Otağ ' + sürüm
let tamga = read('./betik/tamga.js').vars({açıklama})

let yiv = uglify.minify('dist/otag.js', {
  output: {
    ascii_only: true
  },
  ecma: 8,
  compress: {
    pure_funcs: ['makeMap']
  },
  toplevel: true,
  compress: {
    global_defs: {
      '@console.log': 'alert'
    },
    passes: 2
  },
  output: {
    beautify: false,
    preamble: '/* uglified */'
  }
}).code

let Size = s => Math.round(s / 10.24) / 100
fs.writeFile('dist/o.min.js',
`açıklama₺
  * https://otagjs.org
  */

yiv₺`.vars({açıklama,yiv}), err => {
  let boyut = Size(yiv.length)

  zlib.gzip(yiv, (err, zip) => {
    let gzip =  Size(zip.length)

    console.log(`
        tamga₺

        Boyut    : boyut₺ KB
        GZip     : gzip₺ KB

      `.vars({tamga, boyut, gzip}))
    if(!(sürüm.split('.')[2])){
      sürüm = sürüm.split('.')
      sürüm.pop()
      sürüm = sürüm.join('.')
      sürümAna = {
        sürüm, boyut, gzip
      }
      write('./betik/anaSürüm.json',JSON.stringify(sürümAna,null,1))
    }

    README=README.vars({
      boyut, gzip, sürüm,
      boyutAna: sürümAna.boyut,
      gzipAna: sürümAna.gzip,
      sürümAna: sürümAna.sürüm
    })
    write('./README.MD',README)

  })
})


write('dist/otag.js', tamga + read('dist/otag.js'))